module Main (main) where

import Test.Hspec
import Summit.Parser (Error, program)
import Summit.AST
import Text.Megaparsec (Token, ParseError)
import Text.Megaparsec.Error (parseErrorPretty)
import Control.Arrow (left)
import Data.Either (fromRight)

pretty :: Either (ParseError (Token String) Error) Program -> Either String Program
pretty = left parseErrorPretty

main :: IO ()
main = hspec $ do
  -- FIXME remove or update these tests
  describe "Summit.Parser.program" $ do
    it "parses basic arithmetic" $ do

      (pretty $ program "4*5")
        `shouldBe`
        (Right $ Program [Expression $ BinaryExpr $ Binary (NumberLiteral 4) Times (NumberLiteral 5)])

    it "ignores whitespace in arithmetic" $ do

      (pretty $ program "4 + 5")
        `shouldBe`
        (Right $ Program [Expression $ BinaryExpr $ Binary (NumberLiteral 4) Plus (NumberLiteral 5)])

    it "uses order of operations" $ do

      (pretty $ program "4 * 5 + 6")
        `shouldBe`
        (Right $ Program [Expression
                          $ BinaryExpr $ Binary (BinaryExpr $ Binary (NumberLiteral 4) Times (NumberLiteral 5))
                                       Plus (NumberLiteral 6)])

    it "handles parentheses" $ do

      (pretty $ program "4 * (5 + 6)")
        `shouldBe`
        (Right $ Program [Expression
                          $ BinaryExpr $ Binary (NumberLiteral 4)
                                       Times (BinaryExpr $ Binary (NumberLiteral 5) Plus (NumberLiteral 6))])

    it "parses variable definitions" $ do

      (pretty $ program "a = 2")
        `shouldBe`
        (Right $ Program [Assignment (Id "a") (NumberLiteral 2)])

    it "parses function calls" $ do

      (pretty $ program "f(x)")
        `shouldBe`
        (Right $ Program [Expression $ FnCall (Id "f") [Var $ Id "x"]])

    it "parses function definitions with braces" $ do

      (pretty $ program "f(x) = {\na = x * 2\n}")
        `shouldBe`
        (Right $ Program [FnDef $ Fn (Id "f") [(Id "x")]
                          [Assignment (Id "a") $ BinaryExpr $ Binary (Var $ Id "x") Times (NumberLiteral 2)]])

    it "parses function definitions without braces" $ do

      (pretty $ program "f(x) = x * 2")
        `shouldBe`
        (Right $ Program [FnDef $ Fn (Id "f") [Id "x"]
                          [Expression $ BinaryExpr $ Binary (Var $ Id "x") Times (NumberLiteral 2)]])

    it "handles extra whitespace" $ do

      (pretty $ program "\n\na = \n    2\n\n")
        `shouldBe`
        (Right $ Program [Assignment (Id "a") (NumberLiteral 2)])

    it "parses return statements" $ do

      (pretty $ program "return") `shouldBe` (Right $ Program [Return RetEmpty])

    it "parses return statements with expressions" $ do

      (pretty $ program "return 4") `shouldBe` (Right $ Program [Return $ RetExpr $ NumberLiteral 4])
