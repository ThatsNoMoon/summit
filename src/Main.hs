module Main (main) where

import Options.Applicative
import Data.Semigroup ((<>))

import Summit.Parser (Error, program, program1)
import Summit.AST (Program)
import Summit.Interpreter (interpret)

import Text.Megaparsec (Token)
import qualified Text.Megaparsec.Error as ME (ParseError, parseErrorPretty)

import Control.Monad.Trans.Except (ExceptT, catchE, throwE, runExceptT)
import Control.Monad.Trans.Class (lift)

data Options
  = Execute Source ExecOptions
  | Version

data ExecOptions = Exec | Eval | Parse

data Source
  = Filename String
  | Code String

-- | Parse command-line options
optionsP :: Parser Options
optionsP
  =   Execute
  <$> sourceP
  <*> (evaluateP <|> parseP <|> pure Exec)
  <|> versionP

-- | Required option for the source code to use
sourceP :: Parser Source
sourceP = Filename <$> filenameP <|> Code <$> codeP

-- | Use a file for the source code to use
filenameP :: Parser String
filenameP
  =  argument str
  $  metavar  "FILE"
  <> help     "source file to execute"

-- | Use a string of source code
codeP :: Parser String
codeP
  =  strOption
  $  long    "code"
  <> short   'c'
  <> help    "code to immediately execute"
  <> metavar "CODE"

-- | Evaluate the code (and output the final expression/return value)
evaluateP :: Parser ExecOptions
evaluateP
  =  flag' Eval
  $  long  "evaluate"
  <> short 'e'
  <> help  "evaluate code instead of only executing it"

-- | Only parse the code (do not execute it)
parseP :: Parser ExecOptions
parseP
  =  flag' Parse
  $  long  "parse"
  <> short 'p'
  <> help  "only parse code instead of executing it"

-- | Display version and exit
versionP :: Parser Options
versionP
  =  flag' Version
  $  long  "version"
  <> short 'v'
  <> help  "display version number and exit"

opts :: ParserInfo Options
opts
  =  info (optionsP <**> helper)
  $  fullDesc
  <> header "Summit - a toy programming language"

main :: IO ()
main = entry =<< execParser opts

entry :: Options -> IO ()
entry Version = putStrLn "Summit v 0.1.0"
entry (Execute (Code source) eval) = runProgram eval $ program source
entry (Execute (Filename filename) eval)
  = runProgram eval $ program1 filename =<< (lift $ readFile filename)
  
-- | Helper to run program with the given options
runProgram :: ExecOptions
           -> ExceptT (ME.ParseError (Token String) Error) IO Program
           -> IO ()
runProgram Parse res
  =   putStrLn
  =<< (either show show
       <$> (runExceptT $ prettyParseError res))
  where
    prettyParseError e  = catchE e (throwE . ME.parseErrorPretty)
runProgram exec res 
  = either putStrLn (printer . show)
  =<< (runExceptT
       $ (prettyInterpError . interpret) =<< prettyParseError res)
  where
    printer = case exec of
      Exec -> flip seq $ pure ()
      Eval -> putStrLn
      _    -> undefined

    prettyParseError e  = catchE e (throwE . ME.parseErrorPretty)
    prettyInterpError e = catchE e (throwE . show)
