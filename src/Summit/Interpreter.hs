{-# LANGUAGE TupleSections #-}
{-# LANGUAGE LambdaCase #-}

module Summit.Interpreter where

import Summit.AST
import Summit.Interpreter.Types
import Summit.Util

import Control.Monad.Trans.State.Lazy
import Control.Monad.Trans.Except
import Control.Monad.Trans.Class (lift)
import Control.Monad.IO.Class (liftIO)
import Control.Monad ((<=<))
import Data.Foldable (find)
import Data.List (intercalate)
import Control.Applicative ((<|>))
import Lens.Micro.Platform

import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HM (lookup, insert, fromList)
import qualified Data.List.NonEmpty as NE (toList)

-- | Invoke a builtin or user-defined function within Summit
invokeFn :: Fn -> [Value] -> InterpM Value
invokeFn (BuiltinFn b) args = do
  st <- get
  let result = handleEarlyReturn $ evalStateT (invokeBuiltin b args) st
  lift result
invokeFn (UserFn f) args = do
  st <- get
  let result = handleEarlyReturn $ evalStateT (invokeUserFn f args) st
  lift result

-- | Invoke a builtin Summit function
invokeBuiltin :: Builtin -> [Value] -> InterpM Value
invokeBuiltin (Constructor (UserType _ members initializer metatable)) args
  = let table = Table $ HM.fromList
                    (("metatable", Table metatable)
                     : ((, Unit) . getIdName <$> members));
        initArgs = table : args
    in invokeFn initializer initArgs *> pure table
invokeBuiltin DebugState []
  =  pure Unit
  <* (liftIO . print =<< get)
invokeBuiltin DebugState as
  = wrongNumParamsError (Id "debug state") 0 (length as)
invokeBuiltin Print xs
  = pure Unit <* (liftIO $ putStrLn $ intercalate " " (show <$> xs))

-- | Invoke a user-defined Summit function
invokeUserFn :: FnDef -> [Value] -> InterpM Value
invokeUserFn (UserFnDef id_ params body) args = do
  if length params /= length args
      then wrongNumParamsError id_ (length params) (length args)
      else do

      fns <- getFunctions <$> get

      let fnSt = St body fns (builtinBindings <> (HM.fromList
                              $ zip params
                              $ zipWith Binding params args))

      lift $ evalStateT bodyI fnSt

-- | Handle early return conditions, getting the value returned in the case
-- of an early return, and leaving other errors unchanged
handleEarlyReturn :: Monad m
                  => ExceptT InterpCondition m Value
                  -> ExceptT InterpCondition m Value
handleEarlyReturn e = catchE e handle
  where
    handle (EarlyReturn v) = pure v
    handle err             = throwE err

-- | All the Summit builtins
builtinFns :: HashMap Id Fn
builtinFns
  = HM.fromList
  [ (Id "debug state", BuiltinFn DebugState)
  , (Id "print",       BuiltinFn Print)
  ]

builtinBindings :: HashMap Id Binding
builtinBindings = mempty

-- |'interpret' accepts a 'Program' created by the Summit parser, and executes
-- it. 
interpret :: Program -> ExceptT InterpCondition IO Value
interpret (Program stmts) = handleEarlyReturn $ evalStateT bodyI st
  where
    st = St stmts builtinFns builtinBindings

-- | Interpret a body of code. This can include the top level body, the body of
-- an if statement, function, etc.
bodyI :: InterpM Value
bodyI = do
  st <- get
  case getStatements st of
    [] -> pure Unit

    (Expression expr):[] -> exprI expr

    s:_ -> statementI s *> zoom statements popStatement *> bodyI

-- | Interpret a single statement, e.g. function definition, assignment, etc.
statementI :: Statement -> InterpM ()
statementI (Assignment id_ expr) = (setBinding . Binding id_) =<< exprI expr

statementI (FnDef fn@(UserFnDef id_ _ _)) = setFn id_ (UserFn fn)

statementI (TypeDecl (UserTypeDecl id_ members initializer methods))
  = let userType = UserType
                   id_
                   members
                   (UserFn initializer)
                   (HM.fromList
                     $ zip (getIdName . getFnDefId <$> methods)
                     $ Fn . UserFn <$> methods)
    in setFn id_ (BuiltinFn $ Constructor userType)

statementI (Expression expr)       = seq <$> exprI expr <*> pure ()
statementI (Return (RetExpr expr)) = earlyReturn =<< exprI expr
statementI (Return (RetEmpty))     = earlyReturn Unit

-- | Interpret a single expression, e.g. 1 + 4, f(), etc.
exprI :: Expression -> InterpM Value
exprI (NumberLiteral n) = pure $ Number n
exprI (StringLiteral s) = pure $ String s
exprI (BoolLiteral b)   = pure $ Bool b
exprI (ListLiteral l)   = List  <$> traverse exprI l
exprI (TableLiteral t)  = Table <$> traverse exprI t

exprI (Path e p) = atV <$> exprI e <=<> pure p

exprI (BinaryExpr (Binary l op r)) = binary op <$> exprI l <=<> exprI r
  where
    binary Exp      = expV
    binary Times    = productV
    binary Divide   = quotientV
    binary Plus     = sumV
    binary Minus    = differenceV
    binary Concat   = concatV
    binary Equal    = eqV
    binary NotEqual = (notV <=<) . eqV
    binary And      = andV
    binary Or       = orV

exprI (UnaryExpr (Unary op expr)) = unary op =<< exprI expr
  where
    unary Positive = positiveV
    unary Negative = negativeV
    unary Not      = notV

exprI (Var id_)
  =   maybe (unknownVariableError id_) (pure . getValue)
  =<< HM.lookup id_ <$> getBindings <$> get

exprI (FnCall (Var id_) args)
  = invokeFn <$> getFn id_ <=<> (sequence $ exprI <$> args)

exprI (FnCall expr args)
  =    invokeFn
  <$>  (requireFn "function call" =<< exprI expr)
  <=<> (sequence $ exprI <$> args)

exprI (If ifs else_) = do
  condExprs <- sequence $ (exprI . getIfCond) <$> ifs
  conds <- traverse (\case
                        Bool b -> pure b
                        other -> typeError
                            "if" "boolean" other)
    $ condExprs

  let ifs' = zip (NE.toList conds) (NE.toList $ getIfBody <$> ifs)

  fns <- getFunctions <$> get
  binds <- getBindings <$> get

  case find fst ifs' of
    Just (_, body) -> let blockSt = St body fns binds in
      lift $ evalStateT bodyI blockSt

    Nothing -> maybe (pure Unit) (\(ElseExpr body) ->
                                    let blockSt = St body fns binds in
                                     lift $ evalStateT bodyI blockSt) else_

-- | Interpret a path expression, e.g. table.value
atV :: Value -> Pathspec -> InterpM Value
atV (Table t) (IdPath (Id s))       = atTable t s
atV (Table t) (NumberPath n)        = atTable t (show n)
atV (List l) (NumberPath f)         = let n = floor f in
  maybe (indexError (length l) n) pure $ l ^? ix n

atV (List _) (IdPath _)             = unknownError
atV l _                             = typeError "." "table or list" l

-- | Helper function to get the value from at a table with a given property name
atTable :: Table -> String -> InterpM Value
atTable t s = do
  let ownProperty = HM.lookup s t
  metatable <- traverse (requireTable "metatable") $ HM.lookup "metatable" t
  let metatableProperty = HM.lookup s =<< metatable
  maybe unknownError pure $ ownProperty <|> metatableProperty

type BinaryOpV = Value -> Value -> InterpM Value

-- | Higher-order function to create binary operations on Summit number values
-- from Haskell arithmetic functions
arithOp :: String
        -> (Float -> Float -> Float)
        -> BinaryOpV
arithOp _    op (Number l) (Number r) = pure $ Number (op l r)
arithOp name _  (Number _)  r         = typeError name "number" r
arithOp name _   l          _         = typeError name "number" l

-- | Interpret exponentiation
expV        :: BinaryOpV
-- | Interpret multiplication
productV    :: BinaryOpV
-- | Interpret division
quotientV   :: BinaryOpV
-- | Interpret addition
sumV        :: BinaryOpV
-- | Interpret subtraction
differenceV :: BinaryOpV
expV        = arithOp "^" (**)
productV    = arithOp "*" (*)
quotientV   = arithOp "/" (/)
sumV        = arithOp "+" (+)
differenceV = arithOp "-" (-)

-- | Interprete concatenation of strings, lists, or tables
concatV :: BinaryOpV
concatV (String l) (String r) = pure $ String (l <> r)
concatV (Table l)  (Table r)  = pure $ Table  (l <> r)
concatV (List l)   (List r)   = pure $ List   (l <> r)

concatV (String _)  r         = typeError "<>" "string" r
concatV (Table _)   r         = typeError "<>" "table"  r
concatV (List _)    r         = typeError "<>" "list"   r

concatV  l          _         = typeError "<>" "string, list or table" l

-- | Determine equality of two values
eqV :: BinaryOpV
eqV (Number l) (Number r) = eqV' l r
eqV (String l) (String r) = eqV' l r
eqV (Bool l)   (Bool r)   = eqV' l r
eqV (List l)   (List r)   = eqV' l r
eqV  Unit       Unit      = pure $ Bool True
eqV  l          r         = typeError "==" (typeNameV l) r

-- | Helper function to determine equality of two haskell values and wrap
-- within Summit types
eqV' :: Eq a => a -> a -> InterpM Value
eqV' l r = pure $ Bool (l == r)

-- | Interpret logical AND
andV :: BinaryOpV
andV (Bool l) (Bool r) = pure $ Bool (l && r)
andV (Bool _)  r       = typeError "&&" "boolean" r
andV  l        _       = typeError "&&" "boolean" l

-- | Interpret logical OR
orV :: BinaryOpV
orV (Bool l) (Bool r) = pure $ Bool (l || r)
orV (Bool _)  r       = typeError "||" "boolean" r
orV  l        _       = typeError "||" "boolean" l

type UnaryOpV = Value -> InterpM Value

-- | Interpret logical NOT
notV :: UnaryOpV
notV (Bool x) = pure $ Bool (not x)
notV  other   = typeError "!" "boolean" other

-- | Interpret unary plus (multiplication by positive 1)
positiveV :: UnaryOpV
positiveV v@(Number _) = pure v
positiveV v            = typeError "+" "number" v

-- | Interpret unary negation (multiplication by negative 1)
negativeV :: UnaryOpV
negativeV (Number n) = pure $ Number (-n)
negativeV  v         = typeError "-" "number" v

-- | Get the type name of a literal
typeNameE :: Expression -> String
typeNameE (NumberLiteral _) = "number"
typeNameE (StringLiteral _) = "string"
typeNameE (BoolLiteral _)   = "bool"
typeNameE (ListLiteral _)   = "list"
typeNameE (TableLiteral _)  = "table"
typeNameE  _                = "expr"

-- | Require that argument is a table, signals a type error otherwise
requireTable :: String -> Value -> InterpM Table
requireTable _ (Table t) = pure t
requireTable s v         = typeError s "table" v

-- | Require that the argument is a function, signals a type error otherwise
requireFn :: String -> Value -> InterpM Fn
requireFn _ (Fn f) = pure f
requireFn s v      = typeError s "function" v

-- | Set a variable binding in the current state
setBinding :: Binding -> InterpM ()
setBinding bind@(Binding id_ _) = modify $ \(St stmts fns binds) ->
  let newBinds = HM.insert id_ bind binds in St stmts fns newBinds

-- | Set a function binding in the current state
setFn :: Id -> Fn -> InterpM ()
setFn id_ fn = modify $ \(St stmts fns binds) ->
  let newFns = HM.insert id_ fn fns in St stmts newFns binds

-- | Get a function with the given id in the current state, signal an unknown
-- function error if one does not exist
getFn :: Id -> InterpM Fn
getFn id_
  = maybe
    (unknownFunctionError id_)
    pure
    =<< HM.lookup id_ <$> getFunctions <$> get
