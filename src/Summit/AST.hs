module Summit.AST where

import Data.Hashable
import Data.List.NonEmpty (NonEmpty)
import Data.HashMap.Strict (HashMap)

type Statements = [Statement]

data Program = Program Statements deriving (Eq, Show)

type ParamList = [Id]

data Statement
  = Assignment Id Expression
  | FnDef FnDef
  | TypeDecl UserTypeDecl
  | Expression Expression
  | Return Return
  deriving (Eq, Show)

data Id = Id
  { getIdName :: String
  }
  deriving (Eq, Show)

data FnDef = UserFnDef
  { getFnDefId     :: Id
  , getFnDefParams :: ParamList
  , getFnDefStmts  :: Statements
  }
  deriving (Eq, Show)

data UserTypeDecl = UserTypeDecl
  { getTypeId      :: Id
  , getMembers     :: [Id]
  , getInit        :: FnDef
  , getMethods     :: [FnDef]
  }
  deriving (Eq, Show)

data IfExpr = IfExpr
  { getIfCond :: Expression
  , getIfBody :: Statements
  }
  deriving (Eq, Show)

newtype ElseExpr = ElseExpr Statements
  deriving (Eq, Show)

instance Hashable Id where
  hashWithSalt salt (Id s) = hashWithSalt salt s

data Expression
  = UnaryExpr Unary
  | BinaryExpr Binary
  | Path Expression Pathspec
  | NumberLiteral Float
  | StringLiteral String
  | BoolLiteral Bool
  | ListLiteral [Expression]
  | TableLiteral (HashMap String Expression)
  | FnCall Expression [Expression]
  | If (NonEmpty IfExpr) (Maybe ElseExpr)
  | Var Id
  deriving (Eq, Show)

data Unary = Unary UnaryOp Expression
  deriving (Eq, Show)

data Binary = Binary Expression BinaryOp Expression
  deriving (Eq, Show)

data UnaryOp
  = Positive
  | Negative
  | Not
  deriving (Eq, Show)

data BinaryOp
  = Exp
  | Times
  | Divide
  | Plus
  | Minus
  | Concat
  | Equal
  | NotEqual
  | And
  | Or
  deriving (Eq, Show)

data Pathspec
  = NumberPath Float
  | IdPath Id
  deriving (Eq, Show)

data Return
  = RetExpr Expression
  | RetEmpty
  deriving (Eq, Show)
