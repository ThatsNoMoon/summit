module Summit.Util where

import Control.Monad (join)

infixl 4 <=<>

-- |Somewhat like <*> and =<< had a baby. Binds a function already lifted inside
--  a monad. Useful for monadic functions with more than one argument.
(<=<>) :: Monad m => m (a -> m b) -> m a -> m b
f <=<> m = join $ f <*> m
