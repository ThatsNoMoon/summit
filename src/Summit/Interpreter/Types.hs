module Summit.Interpreter.Types where

import Summit.AST

import Control.Monad.Trans.State.Lazy
import Control.Monad.Trans.Except
import Control.Monad.Trans.Class (lift)
import Control.Monad ((<=<))
import Lens.Micro.Platform (lens, Lens', (^?), each)

import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HM

type InterpM = StateT St (ExceptT InterpCondition IO)

data St = St
  { getStatements :: [Statement]
  , getFunctions  :: HashMap Id Fn
  , getBindings   :: HashMap Id Binding
  }
  deriving (Show, Eq)

statements :: Lens' St [Statement]
statements = lens getStatements (\st stmts -> st { getStatements = stmts } )

-- | Remove the first statement of the current state, returning it if
-- one exists. Leaves the state unchanged if it was empty
popStatement :: Monad m => StateT [Statement] m (Maybe Statement)
popStatement = do
  stmts <- get
  case stmts of
    h:t -> put t *> pure (Just h)
    [] -> pure Nothing

data Binding = Binding
  { getID    :: Id
  , getValue :: Value
  }
  deriving (Show, Eq)

data Fn
  = UserFn FnDef
  | BuiltinFn Builtin
  deriving (Show, Eq)

data Builtin
  = Constructor UserType
  | DebugState
  | Print
  deriving (Show, Eq)

data Value
  = Number Float
  | String String
  | Bool Bool
  | List [Value]
  | Unit
  | Table Table
  | Fn Fn
  deriving (Show, Eq)

-- | Get the type name of a value
typeNameV :: Value -> String
typeNameV (Number _) = "number"
typeNameV (String _) = "string"
typeNameV (Bool _)   = "boolean"
typeNameV (List _)   = "list"
typeNameV (Table _)  = "table"
typeNameV  Unit      = "unit"
typeNameV (Fn _)     = "function"

data UserType = UserType
  { getUserTypeName        :: Id
  , getUserTypeMembers     :: [Id]
  , getUserTypeInitializer :: Fn
  , getUserTypeMetatable   :: Table
  }
  deriving (Show, Eq)

type Table = HashMap String Value

data InterpCondition
  = EarlyReturn Value
  | Type TypeError
  | FunctionCall FunctionCallError
  | Index IndexError
  | UnknownVariable UnknownVariableError
  | UnknownFunction UnknownFunctionError
  | Other

instance Show InterpCondition where
  show (EarlyReturn _)     = "Early return"
  show Other               = "Other Error"
  show (Type e)            = "Type Error: " ++ show e
  show (UnknownVariable e) = "Unknown Variable: " ++ (show $ getVariableID e)
  show (UnknownFunction e) = "Unknown Function: " ++ (show $ getFunctionID e)
  show (FunctionCall e)    = "Error in function call: " ++ show e
  show (Index e)           = "Error indexing list: " ++ show e

data TypeError = TypeError
  { getSource   :: Maybe String
  , getExpected :: String
  , getActual   :: String
  }

instance Show TypeError where
  show e
    =  source
    ++ "expected a " ++ getExpected e
    ++ " but got a " ++ getActual e
    where source = maybe "" (++ " ") (getSource e)

data FunctionCallError
  = IncorrectArgumentsError
    { getAttemptedFunctionCallName :: Id
    , getExpectedNumberOfArguments :: Int
    , getActualNumberOfArguments   :: Int
    }

instance Show FunctionCallError where
  show (IncorrectArgumentsError (Id name) expected actual)
    =  name
    ++ " expected " ++ show expected
    ++ " arguments but got " ++ show actual

data IndexError = IndexError
  { getActualLength   :: Int
  , getRequestedIndex :: Int
  }

instance Show IndexError where
  show (IndexError len idx)
    =  "attempted to get index " ++ show idx
    ++ " in a list of length " ++ show len

data UnknownVariableError = UnknownVariableError
  { getVariableID :: Id
  }

data UnknownFunctionError = UnknownFunctionError
  { getFunctionID :: Id
  }

-- | Signal an early return condition with the given value
earlyReturn :: Value -> InterpM a
earlyReturn v = lift $ throwE $ EarlyReturn v

-- | Signal an index error. The first argument is the length of the list
-- that was indexed, the second is the index that was accessed
indexError :: Int -> Int -> InterpM a
indexError l i = lift $ throwE $ Index $ IndexError l i

-- | Signal a type error. The first argument is the source of the type error,
-- the second is the expected type, and the third is the value that was received
typeError :: String -> String -> Value -> InterpM a
typeError s e a = lift $ throwE $ Type $ TypeError (Just s) e (typeNameV a)

-- | Signal a type error. The first argument is an optional source of the type
-- error, the second is the expected type, and the third is the expected type
typeError1 :: Maybe String -> String -> String -> InterpM a
typeError1 s e a = lift $ throwE $ Type $ TypeError s e a

-- | Signal an unknown variable error, when a variable with the given id is
-- used before having been defined
unknownVariableError :: Id -> InterpM a
unknownVariableError id_
  = lift $ throwE $ UnknownVariable $ UnknownVariableError id_

-- | Signal an unknown function error, when a function with the given id is
-- used before having been defined
unknownFunctionError :: Id -> InterpM a
unknownFunctionError id_
  = lift $ throwE $ UnknownFunction $ UnknownFunctionError id_

-- | Signal a wrong number of parameters error, when calling a function with
-- the given id. The first argument is the number of arguments expected by the
-- function, and the second is the actual number of arguments passed
wrongNumParamsError :: Id -> Int -> Int -> InterpM a
wrongNumParamsError n e a
  = lift $ throwE $ FunctionCall $ IncorrectArgumentsError n e a

-- | Signal an unknown error. This should not happen ideally, but is convenient
-- before a suitable error is implemented
unknownError :: InterpM a
unknownError = lift $ throwE $ Other
