module Summit.Parser where

import Summit.AST

import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Error (ShowErrorComponent)
import Text.Read (readMaybe)
import Data.Foldable (asum)
import Control.Monad (void)
import Control.Monad.Trans.Except
import Data.List.NonEmpty (NonEmpty)
import qualified Data.List.NonEmpty as NE
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HM

type Parser a = Parsec Error String a

newtype Error = Error String
  deriving (Eq, Ord, Show)

instance ShowErrorComponent Error where
  showErrorComponent (Error s) = s

-- | Eats many spaces & tabs, runs the given parser, and eats many
-- spaces & tabs again
spaces :: Parser a -> Parser a
spaces p
  =   (try $ skipMany $ char ' ' <|> tab)
   *> p
  <*  (skipMany $ char ' ' <|> tab)

-- | Eats whitespace, runs the given parser, and eats whitespace again
anySpaces :: Parser a -> Parser a
anySpaces p = try space *> p <* space

-- | Parses a program from the given input source code
program :: Monad m
        => String
        -> ExceptT (ParseError (Token String) Error) m Program
program = ExceptT . pure . parse programP "input"

-- | Parses a program with the given source name from the given source code
program1 :: Monad m
         => String
         -> String
         -> ExceptT (ParseError (Token String) Error) m Program
program1 name = ExceptT . pure . parse programP name

-- | Parses the statements of a program
programP :: Parser Program
programP = Program <$> statementsP <* eof

-- | Parses a single terminator (end of line or semicolon)
terminatorP :: Parser ()
terminatorP = (void $ spaces eol) <|> (void $ spaces $ char ';')

-- | Parses many statements separated by terminators
statementsP :: Parser Statements
statementsP = many terminatorP *> statementP `sepEndBy` (some $ try terminatorP)

-- | Parses a single statement, e.g. return, function definition, assignment, etc.
statementP :: Parser Statement
statementP
  =   try (Return   <$> returnP   <?> "a return statement")
  <|> try (FnDef    <$> fnP       <?> "a function definition")
  <|> try (TypeDecl <$> typeDeclP <?> "a type declaration")
  <|> try ((Assignment
            <$> (idP <?> "a variable name") <* anySpaces (char '=')
            <*> expressionP) <?> "an assignment")
  <|> Expression <$> (expressionP <?> "an expression")

-- | Parses a return statement, empty or with a value
returnP :: Parser Return
returnP
  =  string "return"
  *> (try (RetExpr <$> (space1 *> expressionP <* (skipMany (char ' ' <|> tab))))
      <|> pure RetEmpty)

-- | Parses a function definition
fnP :: Parser FnDef
fnP
  =   UserFnDef
  <$> idP
  <*> paramListP <* anySpaces (char '=')
  <*> fnBodyP

-- | Parses the parameter list of a function definition
paramListP :: Parser ParamList
paramListP
  =  spaces (char '(')
  *> (idP <?> "a parameter") `sepBy` spaces (char ',')
  <* spaces (char ')')

-- | Parses the body of a function definition
fnBodyP :: Parser Statements
fnBodyP
  =   anySpaces (char '{')
      *> (statementsP <?> "a function body")
      <* space <* char '}'
  <|> pure <$> Expression <$> (expressionP <?> "an expression")

-- | Parses an identifier possibly including spaces
idP :: Parser Id
idP = Id <$> words_
  where
    word :: Parser String
    word = (:) <$> letterChar <*> many idChar <* optional (char ' ')
    idChar :: Parser Char
    idChar = alphaNumChar <|> char '_'
    words_ :: Parser String
    words_ = unwords <$> some word

-- | Parses a user type declaration
typeDeclP :: Parser UserTypeDecl
typeDeclP
  =   UserTypeDecl
  <$> idP
  <*  anySpaces (char '{')
  <*> membersP
  <*> anySpaces initP
  <*> anySpaces methodsP
  <*  space
  <*  char '}'
  where
    membersP = try idP `sepEndBy` terminatorP
    initP
      =   UserFnDef
      <$> (Id <$> string "init")
      <*> paramListP <* space <* char '='
      <*> fnBodyP
    methodsP = fnP `sepEndBy` terminatorP

-- | Parses a list literal, surrounded by brackets and delimeted
-- by commas
listLiteralP :: Parser [Expression]
listLiteralP
  =   char '['
   *> space
   *> expressionP `sepEndBy` (try $ anySpaces $ char ',')
  <*  space
  <*  char ']'

-- | Parses a table literal, surrounded by braces, delimeted
-- by commas, with = between keys and values
tableLiteralP :: Parser (HashMap String Expression)
tableLiteralP
  =   char '{'
   *> space
   *> (HM.fromList <$>
       ((,)
        <$> (getIdName <$> idP)
        <*> (anySpaces (char '=') *> expressionP))
       `sepEndBy` terminatorP)
  <*  space
  <*  char '}'

-- | Parses a string literal, surrounded by double quotes
stringLiteralP :: Parser String
stringLiteralP = (char '"' *> quoted <* char '"') <?> "a string literal"
  where
    quoted = many quotedChar
    quotedChar
      =   (char '\\' *> (char '"' <|> char '\\'))
      <|> notChar '"'

-- | Parses a number literal, with the same format as a Haskell float
numberLiteralP :: Parser Float
numberLiteralP
  = (maybeP =<< readMaybe <$> many numberChar) <?> "a number literal"
  where
    maybeP = maybe empty pure

-- | Parses a boolean literal, "true" or "false"
boolLiteralP :: Parser Bool
boolLiteralP
  =   True  <$ string "true"
  <|> False <$ string "false"

{-
Binary Operators (planned):
. (accessor)
^ (exponentiation)
*, /
+, -
<>
==, /=
&&
||
-}

-- | Table of binary operators (excluding ".") in order of
-- descending precedence
opPrecTable :: [[(BinaryOp, String)]]
opPrecTable =
  [ [ (Exp, "^")
    ]

  , [ (Times,  "*")
    , (Divide, "/")
    ]

  , [ (Plus,  "+")
    , (Minus, "-")
    ]

  , [ (Concat, "<>")
    ]

  , [ (Equal,    "==")
    , (NotEqual, "/=")
    ]

  , [ (And, "&&")
    ]

  , [ (Or, "||")
    ]
  ]

-- | Creates a parser to parse binary operators with the given
-- constructors and tokens
binaryOpP :: [(BinaryOp, String)] -> Parser BinaryOp
binaryOpP ops
  = asum
  $ (\(op, s) -> op <$ spaces (string s)) <$> ops

-- | Precedence climber, attempting to parse an expression with the
-- first given parser, and then a binary operator with the second
-- given parser
precP :: Parser Expression -> Parser BinaryOp -> Parser Expression
precP exprP opP = precP' =<< exprP
  where
    precP' :: Expression -> Parser Expression
    precP' node
      = (precP'
         =<< BinaryExpr
         <$> (Binary node <$> opP <*> exprP))
      <|> pure node

-- | Parse a single expression
expressionP :: Parser Expression
expressionP = expressionP' =<< binaryP
  where
    expressionP' :: Expression -> Parser Expression
    expressionP' node
      = (expressionP'
         =<< Path node <$> (atP *> pathP))
        <|> pure node
    atP :: Parser ()
    atP = void $ spaces $ char '.'
    pathP :: Parser Pathspec
    pathP
      =   NumberPath <$> numberLiteralP
      <|> IdPath <$> idP

-- | Parse a binary operator expression
binaryP :: Parser Expression
binaryP
  = foldl precP factorP
  $ binaryOpP <$> opPrecTable

-- | Parse a unary operator expression
unaryOpP :: Parser UnaryOp
unaryOpP
  =   Positive <$ spaces (char '+')
  <|> Negative <$ spaces (char '-')
  <|> Not      <$ spaces (char '!')

-- | Parse an if expression
ifExprP :: Parser (NonEmpty IfExpr)
ifExprP = NE.fromList <$> singleIfP `sepBy1` (try $ spaces $ string "or")
  where
    singleIfP :: Parser IfExpr
    singleIfP
      =   IfExpr
      <$> (spaces (string "if")
            *> spaces (char '(')
            *> (expressionP <?> "a condition")
           <*  spaces (char ')')
           <*  anySpaces (char '{'))
      <*> statementsP
      <*  space <* char '}'

-- | Parse an optional else block (after an if block)
elseExprP :: Parser (Maybe ElseExpr)
elseExprP
  =   optional
  $   ElseExpr
  <$> (spaces (string "else")
      *> anySpaces (char '{')
      *> statementsP
      <* spaces (char '}'))


-- | Parse an expression factor, e.g. literals, if expressions, etc.
factorP :: Parser Expression
factorP
  =   UnaryExpr     <$> (Unary <$> unaryOpP <*> factorP)
  <|> NumberLiteral <$> spaces (numberLiteralP)
  <|> StringLiteral <$> spaces (stringLiteralP)
  <|> BoolLiteral   <$> spaces (boolLiteralP)
  <|> ListLiteral   <$> spaces (listLiteralP)
  <|> TableLiteral  <$> spaces (tableLiteralP)
  <|> If            <$> ifExprP <*> elseExprP
  <|> try (FnCall
           <$> Var <$> spaces idP <* spaces (char '(')
           <*> (expressionP <?> "a function argument")
            `sepBy` spaces (char ',')
            <* spaces (char ')'))
  -- <|> try (FnCall
  --          <$> spaces expressionP
  --          <*  spaces (char '(')
  --          <*> (expressionP <?> "a function argument")
  --               `sepBy` spaces (char ',')
  --          <*  spaces (char ')'))
  <|> Var <$> (idP <?> "a variable name")
  <|> (many $ char ' ' <|> tab)
     *> char '('
     *> space
     *> expressionP
    <*  space
    <*  char ')'
    <*  (many $ char ' ' <|> tab)
